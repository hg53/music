/*
 * midi2mus.c -- MIDI to music file converter
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "music.h"


#define OFMT_BIN	0	/* output format is binary */
#define OFMT_ASM	1	/* output format is assembler */
#define OFMT_C		2	/* output format is C */


/**************************************************************/


typedef unsigned char Byte;
typedef unsigned short Half;
typedef unsigned int Word;


typedef enum { false, true } Bool;


/**************************************************************/


Bool showMidi;
Bool showMusic;
Bool showDebug;

FILE *musFile;
int ofmt;

Word globalTime;	/* measured in ticks */
int droppedNotes;

Half format;
Half ntrks;
Half division;


/**************************************************************/


void error(char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  printf("Error: ");
  vprintf(fmt, ap);
  printf("\n");
  va_end(ap);
  exit(1);
}


void warning(char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  printf("Warning: ");
  vprintf(fmt, ap);
  printf("\n");
  va_end(ap);
}


void *memAlloc(unsigned int size) {
  void *mem;

  mem = malloc(size);
  if (mem == NULL) {
    error("out of memory");
  }
  return mem;
}


void memFree(void *mem) {
  free(mem);
}


/**************************************************************/


Half conv2(Byte *p) {
  Half b0, b1;

  b0 = *(p + 0);
  b1 = *(p + 1);
  return (b0 << 8) |
         (b1 << 0);
}


Word conv4(Byte *p) {
  Word b0, b1, b2, b3;

  b0 = *(p + 0);
  b1 = *(p + 1);
  b2 = *(p + 2);
  b3 = *(p + 3);
  return (b0 << 24) |
         (b1 << 16) |
         (b2 <<  8) |
         (b3 <<  0);
}


/**************************************************************/


MusHeader header;


void writeAsmHeader(unsigned int numEvents) {
  if (musFile == NULL) {
    return;
  }
  fprintf(musFile, "\t.code\n");
  fprintf(musFile, "\t.align\t4\n");
  fprintf(musFile, "\n");
  fprintf(musFile, "\t.export\tmusicSize\n");
  fprintf(musFile, "\t.export\tmusic\n");
  fprintf(musFile, "\n");
  fprintf(musFile, "musicSize:\n");
  fprintf(musFile, "\t.word\t0x%08X\n", numEvents);
  fprintf(musFile, "\n");
  fprintf(musFile, "music:\n");
}


void writePrelimHeader(void) {
  if (showMusic) {
    printf("music events:\n");
    printf("      time      event voice  param1  param2 \n");
    printf(" -------------------------------------------\n");
  }
  header.musMagic = MUS_MAGIC;
  header.numEvents = 0;
  if (musFile == NULL) {
    return;
  }
  if (ofmt == OFMT_BIN) {
    if (fwrite(&header, sizeof(MusHeader), 1, musFile) != 1) {
      error("cannot write music file");
    }
  } else
  if (ofmt == OFMT_ASM) {
    writeAsmHeader(0);
  } else
  if (ofmt == OFMT_C) {
    /* nothing to do here */
  }
}


void writeFinalHeader(void) {
  if (musFile == NULL) {
    return;
  }
  fseek(musFile, 0, SEEK_SET);
  if (ofmt == OFMT_BIN) {
    if (fwrite(&header, sizeof(MusHeader), 1, musFile) != 1) {
      error("cannot write music file");
    }
  } else
  if (ofmt == OFMT_ASM) {
    writeAsmHeader(header.numEvents);
  } else
  if (ofmt == OFMT_C) {
    /* nothing to do here */
  }
}


char *eventName[] = {
  /* EVENT_NOTE_ON   */  "   note on",
  /* EVENT_NOTE_OFF  */  "  note off",
  /* EVENT_STOP      */  "      stop",
  /* EVENT_SET_INST  */  "  set inst",
  /* EVENT_SET_TEMPO */  " set tempo",
};


void showEvent(MusEvent *ptr) {
  if (ptr->event < 0 ||
      ptr->event >= sizeof(eventName) / sizeof(eventName[0])) {
    error("unknown event type %d in showEvent()", ptr->event);
  }
  printf(" %9d %s   %3d     %3d     %3d\n",
         ptr->time, eventName[ptr->event],
         ptr->voice, ptr->param1, ptr->param2);
}


void writeEvent(MusEvent *event) {
  if (showMusic) {
    showEvent(event);
  }
  if (musFile == NULL) {
    return;
  }
  if (ofmt == OFMT_BIN) {
    if (fwrite(event, sizeof(MusEvent), 1, musFile) != 1) {
      error("cannot write music file");
    }
  } else
  if (ofmt == OFMT_ASM) {
    fprintf(musFile, "\t.word\t0x%08X\n", event->time);
    fprintf(musFile, "\t.byte\t0x%02X, 0x%02X\n",
            event->event, event->voice);
    fprintf(musFile, "\t.byte\t0x%02X, 0x%02X\n",
            event->param1, event->param2);
  } else
  if (ofmt == OFMT_C) {
    fprintf(musFile, "  { ");
    fprintf(musFile, "0x%08X, ", event->time);
    fprintf(musFile, "0x%02X, 0x%02X, ", event->event, event->voice);
    fprintf(musFile, "0x%02X, 0x%02X ", event->param1, event->param2);
    fprintf(musFile, "},\n");
  }
  header.numEvents++;
}


/**************************************************************/


typedef struct {
  char type[4];		/* chunk identifier */
  Word length;		/* chunk data length, in bytes */
  Byte *data;		/* chnunk data proper */
  /*----------*/
  Bool active;		/* chunk is active */
  Byte *dp;		/* chunk data pointer */
  Word time;		/* time for next event */
  Byte runningStatus;	/* MIDI status byte in effect */
} Chunk;


void readChunk(Chunk *chunk, FILE *midiFile, char *expectedType) {
  Byte buf[4];

  if (fread(chunk->type, 1, 4, midiFile) != 4) {
    error("cannot read chunk type");
  }
  if (strncmp(chunk->type, expectedType, 4) != 0) {
    error("chunk type '%s', expected '%s'", chunk->type, expectedType);
  }
  if (fread(buf, 4, 1, midiFile) != 1) {
    error("cannot read chunk length");
  }
  chunk->length = conv4(buf);
  chunk->data = memAlloc(chunk->length);
  if (fread(chunk->data, 1, chunk->length, midiFile) != chunk->length) {
    error("cannot read chunk data");
  }
  if (showDebug) {
    printf("chunk read: type = '%c%c%c%c', length = %u\n",
           chunk->type[0], chunk->type[1], chunk->type[2], chunk->type[3],
           chunk->length);
  }
}


Byte getByte(Chunk *chunk) {
  if (chunk->dp >= chunk->data + chunk->length) {
    error("reading past end of chunk");
  }
  return *(chunk->dp)++;
}


Word getVarLen(Chunk *chunk) {
  Word val;
  Byte b;

  val = 0;
  do {
    b = getByte(chunk);
    val = (val << 7) | (b & 0x7F);
  } while (b & 0x80);
  return val;
}


/**************************************************************/


#define NUM_VOICES	16

#define STATE_OFF	0
#define STATE_ON	1


typedef struct {
  int state;
  int note;
} Voice;

Voice voiceTbl[NUM_VOICES];
int firstVoiceNotAssigned;


void initVoiceTbl(void) {
  int i;

  for (i = 0; i < NUM_VOICES; i++) {
    voiceTbl[i].state = STATE_OFF;
    voiceTbl[i].note = 0;
  }
  firstVoiceNotAssigned = 0;
}


/**************************************************************/


typedef struct {
  int inst;		/* the instrument this channel is playing */
  unsigned int voices;	/* all voices that are assigned to this channel */
} Channel;

Channel channelTbl[16];


void initChannelTbl(void) {
  int i;

  for (i = 0; i < 16; i++) {
    channelTbl[i].inst = 0;
    channelTbl[i].voices = 0;
  }
}


/**************************************************************/


#define TPQ	480	/* synth resolution: ticks per quarter */


Word midiTicks2synthTicks(Word midiTicks) {
  double factor;

  factor =  (double) TPQ / (double) division;
  return (Word) ((double) midiTicks * factor + 0.5);
}


void generateNoteOn(int voice, int note) {
  MusEvent event;
  int octave, relNote;

  event.time = midiTicks2synthTicks(globalTime);
  event.event = EVENT_NOTE_ON;
  event.voice = voice;
  octave = note / 12 - 1;
  if (octave < 0 || octave > 7) {
    /* this note cannot be played by our synth */
    return;
  }
  relNote = note % 12;
  event.param1 = (octave << 4) | relNote;
  event.param2 = 0;
  writeEvent(&event);
}


void generateNoteOff(int voice) {
  MusEvent event;

  event.time = midiTicks2synthTicks(globalTime);
  event.event = EVENT_NOTE_OFF;
  event.voice = voice;
  event.param1 = 0;
  event.param2 = 0;
  writeEvent(&event);
}


void generateStop(void) {
  MusEvent event;

  event.time = midiTicks2synthTicks(globalTime);
  event.event = EVENT_STOP;
  event.voice = 0;
  event.param1 = 0;
  event.param2 = 0;
  writeEvent(&event);
}


void generateSetInst(int voice, int inst) {
  MusEvent event;

  event.time = midiTicks2synthTicks(globalTime);
  event.event = EVENT_SET_INST;
  event.voice = voice;
  event.param1 = inst;
  event.param2 = 0;
  writeEvent(&event);
}


void generateSetTempo(int bpm) {
  MusEvent event;

  event.time = midiTicks2synthTicks(globalTime);
  event.event = EVENT_SET_TEMPO;
  event.voice = 0;
  event.param1 = bpm;
  event.param2 = 0;
  writeEvent(&event);
}


/**************************************************************/


void noteOff(Byte chn, Byte note) {
  unsigned int voices;
  int i;

  if (showMidi) {
    printf("Channel = %u, note off = %u\n", chn, note);
  }
  voices = channelTbl[chn].voices;
  for (i = 0; i < NUM_VOICES; i++) {
    if (voices & (1 << i)) {
      if (voiceTbl[i].state == STATE_ON && voiceTbl[i].note == note) {
        /* turn off this voice */
        generateNoteOff(i);
        voiceTbl[i].state = STATE_OFF;
        return;
      }
    }
  }
}


void noteOn(Byte chn, Byte note, Byte vel) {
  unsigned int voices;
  int i;

  if (showMidi) {
    printf("Channel = %u, note on = %u, vel = %u\n", chn, note, vel);
  }
  /* try to find a voice which is assigned to this channel, but OFF */
  voices = channelTbl[chn].voices;
  for (i = 0; i < NUM_VOICES; i++) {
    if (voices & (1 << i)) {
      if (voiceTbl[i].state == STATE_OFF) {
        /* turn on this voice */
        generateNoteOn(i, note);
        voiceTbl[i].state = STATE_ON;
        voiceTbl[i].note = note;
        return;
      }
    }
  }
  /* try to use the first unassigned voice */
  if (firstVoiceNotAssigned < NUM_VOICES) {
    i = firstVoiceNotAssigned++;
    generateSetInst(i, channelTbl[chn].inst);
    channelTbl[chn].voices |= (1 << i);
    generateNoteOn(i, note);
    voiceTbl[i].state = STATE_ON;
    voiceTbl[i].note = note;
    return;
  }
  /* try to use any voice which is OFF */
  for (i = 0; i < NUM_VOICES; i++) {
    if (voiceTbl[i].state == STATE_OFF) {
      generateSetInst(i, channelTbl[chn].inst);
      channelTbl[chn].voices |= (1 << i);
      generateNoteOn(i, note);
      voiceTbl[i].state = STATE_ON;
      voiceTbl[i].note = note;
      return;
    }
  }
  /* count the drooped notes */
  droppedNotes++;
}


void controlChange(Byte chn, Byte ctrl, Byte val) {
  if (showMidi) {
    printf("Channel = %u, ctrl = %u, val = %u\n", chn, ctrl, val);
  }
}


void programChange(Byte chn, Byte prog) {
  if (showMidi) {
    printf("Channel = %u, prog = %u\n", chn, prog);
  }
}


/**************************************************************/


void initEventDecoder(Chunk *track) {
  track->active = true;
  track->dp = track->data;
  track->time = getVarLen(track);
  track->runningStatus = 0;
}


void handleSysexEvent(int track, Bool impliedF0,
                      Word eventLength, Byte *eventData) {
  warning("Track%d: sysex event (%s implied 'F0', length %u) skipped",
          track, impliedF0 ? "with" : "without", eventLength);
}


void handleMetaEvent(int track, Byte eventType,
                     Word eventLength, Byte *eventData) {
  Byte nn, dd, cc, bb;
  Byte sf, mi;
  int k;
  int i;

  switch (eventType) {
    case 0x00:
      /* Sequence Number */
      if (eventLength != 2) {
        error("Track %2d: meta-event 'Sequence Number' has illegal length %u",
              track, eventLength);
      }
      k = ((Word) eventData[0] << 8) |
          ((Word) eventData[1] << 0);
      if (showMidi) {
        printf("Track %2d: Sequence Number = %u\n", track, k);
      }
      break;
    case 0x01:
      /* Text Event */
      if (showMidi) {
        printf("Track %2d: Text = '", track);
        for (i = 0; i < eventLength; i++) {
          printf("%c", eventData[i]);
        }
        printf("'\n");
      }
      break;
    case 0x02:
      /* Copyright Notice */
      if (showMidi) {
        printf("Track %2d: Copyright Notice = '", track);
        for (i = 0; i < eventLength; i++) {
          printf("%c", eventData[i]);
        }
        printf("'\n");
      }
      break;
    case 0x03:
      /* Sequence/Track Name */
      if (showMidi) {
        printf("Track %2d: Track Name = '", track);
        for (i = 0; i < eventLength; i++) {
          printf("%c", eventData[i]);
        }
        printf("'\n");
      }
      break;
    case 0x04:
      /* Instrument Name */
      if (showMidi) {
        printf("Track %2d: Instrument Name = '", track);
        for (i = 0; i < eventLength; i++) {
          printf("%c", eventData[i]);
        }
        printf("'\n");
      }
      break;
    case 0x05:
      /**/
      warning("meta-event 05 not yet");
      break;
    case 0x06:
      /**/
      warning("meta-event 06 not yet");
      break;
    case 0x07:
      /**/
      warning("meta-event 07 not yet");
      break;
    case 0x20:
      /* MIDI Channel Prefix */
      if (eventLength != 1) {
        error("Track %2d: meta-event 'MIDI Channel' has illegal length %u",
              track, eventLength);
      }
      cc = eventData[0];
      if (showMidi) {
        printf("Track %2d: MIDI Channel = %u\n", track, cc);
      }
      break;
    case 0x21:
      /* MIDI Port */
      if (eventLength != 1) {
        error("Track %2d: meta-event 'MIDI Port' has illegal length %u",
              track, eventLength);
      }
      cc = eventData[0];
      if (showMidi) {
        printf("Track %2d: MIDI Port = %u\n", track, cc);
      }
      break;
    case 0x2F:
      /* End of Track */
      if (eventLength != 0) {
        error("Track %2d: meta-event 'End of Track' has illegal length %u",
              track, eventLength);
      }
      if (showMidi) {
        printf("Track %2d: End of Track\n", track);
      }
      break;
    case 0x51:
      /* Set Tempo */
      if (eventLength != 3) {
        error("Track %2d: meta-event 'Set Tempo' has illegal length %u",
              track, eventLength);
      }
      k = ((Word) eventData[0] << 16) |
          ((Word) eventData[1] <<  8) |
          ((Word) eventData[2] <<  0);
      if (showMidi) {
        printf("Track %2d: Set Tempo = %u microseconds per quarter note\n",
               track, k);
      }
      generateSetTempo((int) ((double) 60000000 / (double) k + 0.5));
      break;
    case 0x54:
      /**/
      warning("meta-event 54 not yet");
      break;
    case 0x58:
      /* Time Signature */
      if (eventLength != 4) {
        error("Track %2d: meta-event 'Time Signature' has illegal length %u",
              track, eventLength);
      }
      nn = eventData[0];
      dd = eventData[1];
      cc = eventData[2];
      bb = eventData[3];
      if (showMidi) {
        printf("Track %2d: Time Signature = %u/%u\n", track, nn, 1 << dd);
        printf("          %u MIDI clocks in a metronome click\n", cc);
        printf("          %u notated 1/32 notes in a quarter note\n", bb);
      }
      break;
    case 0x59:
      /* Key Signature */
      if (eventLength != 2) {
        error("Track %2d: meta-event 'Key Signature' has illegal length %u",
              track, eventLength);
      }
      sf = eventData[0];
      mi = eventData[1];
      k = (int) (signed char) sf;
      if (showMidi) {
        printf("Track %2d: Key Signature = %d %s %s\n",
               track, k < 0 ? -k : k, k < 0 ? "flat(s)" : "sharp(s)",
               mi == 0 ? "major" : "minor");
      }
      break;
    case 0x7F:
      /* Sequencer-Specific Meta-Event */
      warning("Track %2d: Sequencer-Specific Meta-Event (length %u) skipped",
              track, eventLength);
      break;
    default:
      warning("Track %2d: meta-event 0x%02X (length %u) skipped",
              track, eventType, eventLength);
      break;
  }
}


void handleMidiEvent(int track, Byte status, Byte param1, Byte param2) {
  Byte msg, chn;

  msg = status & 0xF0;
  chn = status & 0x0F;
  switch (msg) {
    case 0x80:
      /* Note Off */
      noteOff(chn, param1);
      break;
    case 0x90:
      /* Note On */
      if (param2 == 0) {
        noteOff(chn, param1);
      } else {
        noteOn(chn, param1, param2);
      }
      break;
    case 0xA0:
      /* Polyphonic Key Pressure */
      /* ignored for now */
      break;
    case 0xB0:
      /* Control Change */
      controlChange(chn, param1, param2);
      break;
    case 0xC0:
      /* Program Change */
      programChange(chn, param1);
      break;
    case 0xD0:
      /* Channel Pressure */
      /* ignored for now */
      break;
    case 0xE0:
      /* Pitch Bend Change */
      /* ignored for now */
      break;
    default:
      error("handleMidiEvent() detected illegal MIDI message 0x%02X", msg);
      break;
  }
}


void interpretMidiFile(FILE *midiFile) {
  Chunk *header;
  Chunk *tracks;
  int i;
  int nextTrack;
  Word nextTime;
  Byte b;
  Byte eventType;
  Word eventLength;
  Byte status, param1, param2;

  header = memAlloc(sizeof(Chunk));
  readChunk(header, midiFile, "MThd");
  format = conv2(header->data + 0);
  ntrks = conv2(header->data + 2);
  division = conv2(header->data + 4);
  if (format >= 2) {
    error("cannot interpret MIDI file formats >= 2, sorry");
  }
  if (division & 0x8000) {
    error("cannot decode time-code-based time, sorry");
  }
  if (showMidi) {
    printf("Header: ");
    printf("Format = %hu, ", format);
    printf("Tracks = %hu, ", ntrks);
    printf("Division = %hu ticks per quarter note\n", division);
  }
  tracks = malloc(ntrks * sizeof(Chunk));
  for (i = 0; i < ntrks; i++) {
    readChunk(tracks + i, midiFile, "MTrk");
    initEventDecoder(tracks + i);
  }
  initVoiceTbl();
  initChannelTbl();
  globalTime = 0;
  droppedNotes = 0;
  writePrelimHeader();
  while (1) {
    /* search track with next event */
    nextTrack = -1;
    nextTime = 0x10000000;
    for (i = 0; i < ntrks; i++) {
      /* skip track if inactive */
      if (!tracks[i].active) {
        continue;
      }
      /* else remember (first) most urgent track */
      if (tracks[i].time < nextTime) {
        nextTrack = i;
        nextTime = tracks[i].time;
      }
    }
    if (nextTrack == -1) {
      /* no track active any longer */
      break;
    }
    /* update global time */
    globalTime = nextTime;
    if (showDebug) {
      printf("Track %2d: reading next event (global time is now %u)\n",
             nextTrack, globalTime);
    }
    /* get and handle next event */
    b = getByte(&tracks[nextTrack]);
    if (b == 0xF0 || b == 0xF7) {
      /* sysex event */
      eventLength = getVarLen(&tracks[nextTrack]);
      if (showDebug) {
        printf("Track %2d: %s sysex event, length %u\n",
               nextTrack, b == 0xF0 ? "F0" : "F7", eventLength);
      }
      handleSysexEvent(nextTrack, b == 0xF0,
                       eventLength, tracks[nextTrack].dp);
      tracks[nextTrack].dp += eventLength;
      tracks[nextTrack].runningStatus = 0;
    } else
    if (b == 0xFF) {
      /* meta-event */
      eventType = getByte(&tracks[nextTrack]);
      eventLength = getVarLen(&tracks[nextTrack]);
      if (showDebug) {
        printf("Track %2d: meta-event 0x%02X, length %u\n",
               nextTrack, eventType, eventLength);
      }
      handleMetaEvent(nextTrack, eventType,
                      eventLength, tracks[nextTrack].dp);
      if (eventType == 0x2F) {
        /* End of Track: deactivate this track */
        tracks[nextTrack].active = false;
      }
      tracks[nextTrack].dp += eventLength;
      tracks[nextTrack].runningStatus = 0;
    } else
    if ((b & 0xF0) == 0xF0) {
      /* any other system message */
      error("Track %2d: illegal system message 0x%02X", nextTrack, b);
    } else
    if ((b & 0x80) != 0 || tracks[nextTrack].runningStatus != 0) {
      /* MIDI event: any MIDI channel message */
      if ((b & 0x80) != 0) {
        /* status byte present */
        status = b;
        param1 = getByte(&tracks[nextTrack]);
      } else {
        /* status byte absent, use running status */
        status = tracks[nextTrack].runningStatus;
        param1 = b;
      }
      if ((b & 0xF0) == 0xC0 || (b & 0xF0) == 0xD0) {
        /* 2-byte MIDI message */
        param2 = 0;
      } else {
        /* 3-byte MIDI message */
        param2 = getByte(&tracks[nextTrack]);
      }
      handleMidiEvent(nextTrack, status, param1, param2);
      tracks[nextTrack].runningStatus = status;
    } else {
      /* any data byte outside of running status */
      error("Track %2d: stray data byte 0x%02X", nextTrack, b);
    }
    /* update track time (note: track may have become inactive) */
    if (tracks[nextTrack].active) {
      tracks[nextTrack].time += getVarLen(&tracks[nextTrack]);
      if (showDebug) {
        printf("Track %2d: time updated to %u\n",
               nextTrack, tracks[nextTrack].time);
      }
    }
  }
  generateStop();
  writeFinalHeader();
  printf("%d notes dropped\n", droppedNotes);
}


/**************************************************************/


void usage(char *myself) {
  printf("Usage: %s [options] <MIDI file>\n", myself);
  printf("valid options are:\n");
  printf("    -ob <music file>       specify binary music output file\n");
  printf("    -os <music file>       specify assembler music output file\n");
  printf("    -oc <music file>       specify C music output file\n");
  printf("    -showMidi              show events in MIDI file\n");
  printf("    -showMusic             show events in music file\n");
  printf("    -showDebug             enable debugging output\n");
  exit(1);
}


int main(int argc, char *argv[]) {
  int i;
  char *argptr;
  char *midiFileName;
  char *musFileName;
  FILE *midiFile;

  midiFileName = NULL;
  musFileName = NULL;
  showMidi = false;
  showMusic = false;
  showDebug = false;
  for (i = 1; i < argc; i++) {
    argptr = argv[i];
    if (*argptr == '-') {
      /* option */
      if (strcmp(argptr, "-ob") == 0 ||
          strcmp(argptr, "-os") == 0 ||
          strcmp(argptr, "-oc") == 0) {
        if (i == argc - 1) {
          error("file name for output option missing");
        }
        if (musFileName != NULL) {
          error("file name for output option already set");
        }
        musFileName = argv[++i];
        switch (argptr[2]) {
          case 'b':
            ofmt = OFMT_BIN;
            break;
          case 's':
            ofmt = OFMT_ASM;
            break;
          case 'c':
            ofmt = OFMT_C;
            break;
        }
      } else
      if (strcmp(argptr, "-showMidi") == 0) {
        showMidi = true;
      } else
      if (strcmp(argptr, "-showMusic") == 0) {
        showMusic = true;
      } else
      if (strcmp(argptr, "-showDebug") == 0) {
        showDebug = true;
      } else {
        usage(argv[0]);
      }
    } else {
      /* file */
      if (midiFileName != NULL) {
        error("MIDI file name already set");
      }
      midiFileName = argptr;
    }
  }
  if (midiFileName == NULL) {
    error("no MIDI file");
  }
  midiFile = fopen(midiFileName, "r");
  if (midiFile == NULL) {
    error("cannot open MIDI file '%s'", midiFileName);
  }
  printf("MIDI file: '%s'\n", midiFileName);
  if (musFileName == NULL) {
    musFile = NULL;
  } else {
    musFile = fopen(musFileName, "w");
    if (musFile == NULL) {
      error("cannot open music file '%s'", musFileName);
    }
    printf("Music file: '%s'\n", musFileName);
  }
  interpretMidiFile(midiFile);
  fclose(midiFile);
  if (musFile != NULL) {
    fclose(musFile);
  }
  return 0;
}
