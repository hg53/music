%{

/*
 * genmus.y -- generate music file from score file
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <ctype.h>
#include "music.h"

#define OFMT_BIN	0	/* output format is binary */
#define OFMT_ASM	1	/* output format is assembler */
#define OFMT_C		2	/* output format is C */

#define TICKS_PER_64TH	30	/* resolution is 480 ticks per quarter */

int showLists = 1;
int showMusic = 1;
int lineno = 1;

int currSyncpoint = -1;
int prevSyncpoint = -2;

int duration;
int slur;
int voice;
int note;
int octave;
int instr;

#define CMD_NONE	0
#define CMD_INSTR	1
#define CMD_NOTE_ON	2
#define CMD_NOTE_OFF	3
#define CMD_REST_ON	4
#define CMD_REST_OFF	5
#define CMD_STOP	6
#define CMD_TEMPO	7

typedef struct command {
  int sync;		/* time at wich current measure starts */
  int time;		/* time at which action should happen */
  int type;		/* command type */
  int note;		/* note number */
  int octv;		/* octave number */
  int parm;		/* parameter (instr, tempo, etc.) */
  struct command *next;	/* pointer to next command */
} Command;

#define MAX_VOICES	16	/* maximum number of voices */

Command *cmdlists[MAX_VOICES];
Command *cmdlasts[MAX_VOICES];

Command *cmdptr;

void error(char *fmt, ...);
void yyerror(char *msg);
void *allocate(unsigned int numbytes);
int yylex(void);

%}

%token		NUMBER
%token		NAME
%token		REST
%token		COMMA
%token		COLON
%token		DOT
%token		STAR
%token		PLUS
%token		AT
%token		DOLLAR

%start		score


%%


score		: list
		  {
		    prevSyncpoint = currSyncpoint;
		    currSyncpoint += 64 * TICKS_PER_64TH;
		    cmdptr = (Command *) allocate(sizeof(Command));
		    cmdptr->sync = currSyncpoint;
		    cmdptr->time = currSyncpoint + 1 * TICKS_PER_64TH;
		    cmdptr->type = CMD_STOP;
		    cmdptr->note = 0;
		    cmdptr->octv = 0;
		    cmdptr->parm = 0;
		    cmdptr->next = NULL;
		    cmdlasts[0]->next = cmdptr;
		    cmdlasts[0] = cmdptr;
		  }
		;

list		: element
		| list COMMA element
		;

element		: instrument
		| measure
		| tempo
		| note
		| rest
		;

instrument	: DOLLAR voice COLON instr
		  {
		    cmdptr = (Command *) allocate(sizeof(Command));
		    cmdptr->sync = currSyncpoint;
		    cmdptr->time = currSyncpoint;
		    cmdptr->type = CMD_INSTR;
		    cmdptr->note = 0;
		    cmdptr->octv = 0;
		    cmdptr->parm = instr;
		    cmdptr->next = NULL;
		    cmdlasts[voice]->next = cmdptr;
		    cmdlasts[voice] = cmdptr;
		  }
		;

measure		: STAR
		  {
		    prevSyncpoint = currSyncpoint;
		    currSyncpoint += 64 * TICKS_PER_64TH;
		  }
		| STAR NUMBER
		  {
		    prevSyncpoint = currSyncpoint;
		    currSyncpoint = ($2 - 1) * 64 * TICKS_PER_64TH;
		  }
		;

tempo		: AT NUMBER
		  {
		    cmdptr = (Command *) allocate(sizeof(Command));
		    cmdptr->sync = currSyncpoint;
		    cmdptr->time = currSyncpoint;
		    cmdptr->type = CMD_TEMPO;
		    cmdptr->note = 0;
		    cmdptr->octv = 0;
		    cmdptr->parm = $2;
		    cmdptr->next = NULL;
		    cmdlasts[voice]->next = cmdptr;
		    cmdlasts[voice] = cmdptr;
		  }
		;

note		: voice COLON pitch COLON duration
		  {
		    int time;
		    if (slur == 0) {
		      /* slur absent */
		      if (cmdlasts[voice]->sync == prevSyncpoint) {
		        /* note event adjacent to previous measure */
		        time = currSyncpoint;
		        if (cmdlasts[voice]->type == CMD_NOTE_OFF) {
                          cmdlasts[voice]->time -= TICKS_PER_64TH;
		        }
		      } else
		      if (cmdlasts[voice]->sync == currSyncpoint) {
		        /* note event within current measure */
		        time = cmdlasts[voice]->time;
		        if (cmdlasts[voice]->type == CMD_NOTE_OFF) {
                          cmdlasts[voice]->time -= TICKS_PER_64TH;
		        }
		      } else {
		        /* note event of a freshly starting voice */
		        time = currSyncpoint;
		      }
		      cmdptr = (Command *) allocate(sizeof(Command));
		      cmdptr->sync = currSyncpoint;
		      cmdptr->time = time;
		      cmdptr->type = CMD_NOTE_ON;
		      cmdptr->note = note;
		      cmdptr->octv = octave;
		      cmdptr->parm = 0;
		      cmdptr->next = NULL;
		      cmdlasts[voice]->next = cmdptr;
		      cmdlasts[voice] = cmdptr;
		      cmdptr = (Command *) allocate(sizeof(Command));
		      cmdptr->sync = currSyncpoint;
		      cmdptr->time = cmdlasts[voice]->time + duration;
		      cmdptr->type = CMD_NOTE_OFF;
		      cmdptr->note = note;
		      cmdptr->octv = octave;
		      cmdptr->parm = 0;
		      cmdptr->next = NULL;
		      cmdlasts[voice]->next = cmdptr;
		      cmdlasts[voice] = cmdptr;
		    } else {
		      /* slur present */
		      if (cmdlasts[voice]->sync == prevSyncpoint) {
		        /* note event adjacent to previous measure */
		        time = currSyncpoint;
		        if (cmdlasts[voice]->type == CMD_NOTE_OFF) {
                          cmdlasts[voice]->time -= 2;
		        }
		      } else
		      if (cmdlasts[voice]->sync == currSyncpoint) {
		        /* note event within current measure */
		        time = cmdlasts[voice]->time;
		        if (cmdlasts[voice]->type == CMD_NOTE_OFF) {
                          cmdlasts[voice]->time -= 2;
		        }
		      } else {
		        /* note event of a freshly starting voice */
                        error("misplaced slur in line %d", lineno);
		      }
		      if (cmdlasts[voice]->note != note ||
		          cmdlasts[voice]->octv != octave) {
                        /* slur present, different pitch */
		       cmdptr = (Command *) allocate(sizeof(Command));
		       cmdptr->sync = currSyncpoint;
		       cmdptr->time = time;
		       cmdptr->type = CMD_NOTE_ON;
		       cmdptr->note = note;
		       cmdptr->octv = octave;
		       cmdptr->parm = 0;
		       cmdptr->next = NULL;
		       cmdlasts[voice]->next = cmdptr;
		       cmdlasts[voice] = cmdptr;
		       cmdptr = (Command *) allocate(sizeof(Command));
		       cmdptr->sync = currSyncpoint;
		       cmdptr->time = cmdlasts[voice]->time + duration;
		       cmdptr->type = CMD_NOTE_OFF;
		       cmdptr->note = note;
		       cmdptr->octv = octave;
		       cmdptr->parm = 0;
		       cmdptr->next = NULL;
		       cmdlasts[voice]->next = cmdptr;
		       cmdlasts[voice] = cmdptr;
		      } else {
		        /* slur present, same pitch */
		        if (cmdlasts[voice]->type != CMD_NOTE_OFF) {
		          error("misplaced slur in line %d", lineno);
		        }
		        cmdlasts[voice]->sync = currSyncpoint;
		        cmdlasts[voice]->time += duration + 2;
		      }
		    }
		  }
		;

rest		: voice COLON REST COLON duration
		  {
		    int time;
		    if (cmdlasts[voice]->sync == prevSyncpoint) {
		      /* rest event adjacent to previous measure */
		      time = currSyncpoint;
		      if (cmdlasts[voice]->type == CMD_NOTE_OFF) {
                        cmdlasts[voice]->time -= TICKS_PER_64TH;
		      }
		    } else
		    if (cmdlasts[voice]->sync == currSyncpoint) {
		      /* rest event within current measure */
		      time = cmdlasts[voice]->time;
		      if (cmdlasts[voice]->type == CMD_NOTE_OFF) {
                        cmdlasts[voice]->time -= TICKS_PER_64TH;
		      }
		    } else {
		      /* rest event of a freshly starting voice */
		      time = currSyncpoint;
		    }
		    cmdptr = (Command *) allocate(sizeof(Command));
		    cmdptr->sync = currSyncpoint;
		    cmdptr->time = time;
		    cmdptr->type = CMD_REST_ON;
		    cmdptr->note = 0;
		    cmdptr->octv = 0;
		    cmdptr->parm = 0;
		    cmdptr->next = NULL;
		    cmdlasts[voice]->next = cmdptr;
		    cmdlasts[voice] = cmdptr;
		    cmdptr = (Command *) allocate(sizeof(Command));
		    cmdptr->sync = currSyncpoint;
		    cmdptr->time = cmdlasts[voice]->time + duration;
		    cmdptr->type = CMD_REST_OFF;
		    cmdptr->note = 0;
		    cmdptr->octv = 0;
		    cmdptr->parm = 0;
		    cmdptr->next = NULL;
		    cmdlasts[voice]->next = cmdptr;
		    cmdlasts[voice] = cmdptr;
		  }
		;

voice		: NUMBER
		  {
		    voice = $1 - 1;
		  }
		;

pitch		: NAME NUMBER
		  {
		    note = $1;
		    octave = $2;
		  }
		;

duration	: length
		  {
		    duration = $1 * TICKS_PER_64TH;
		    slur = 0;
		  }
		| PLUS length
		  {
		    duration = $2 * TICKS_PER_64TH;
		    slur = 1;
		  }
		;

length		: NUMBER
		  {
		    $$ = 64 / $1;
		  }
		| NUMBER DOT
		  {
		    $$ = 96 / $1;
		  }
		;

instr		: NUMBER
		  {
		    instr = $1 - 1;
		  }
		;


%%


FILE *scoreFile;


void error(char *fmt, ...) {
  va_list ap;

  printf("Error: ");
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
  printf("\n");
  exit(1);
}


void yyerror(char *msg) {
  printf("\nError in line %d: %s\n", lineno, msg);
  exit(1);
}


void *allocate(unsigned int numbytes) {
  void *ptr;

  ptr = malloc(numbytes);
  if (ptr == NULL) {
    yyerror("not enough space");
  }
  return ptr;
}


/**************************************************************/


int yylex(void) {
  int ch;
  int sign, val;

  while (1) {
    ch = getc(scoreFile);
    if (ch == ' ' || ch == '\t' || ch == '\r') {
      continue;
    }
    if (ch == '#') {
      while (ch != '\n') {
        ch = getc(scoreFile);
      }
      lineno++;
      continue;
    }
    if (ch == '\n') {
      lineno++;
      continue;
    }
    if (ch == '-' || isdigit(ch)) {
      sign = 1;
      if (ch == '-') {
        sign = -1;
        ch = getc(scoreFile);
      }
      val = 0;
      while (isdigit(ch)) {
        val = val * 10 + (ch - '0');
        ch = getc(scoreFile);
      }
      ungetc(ch, scoreFile);
      yylval = sign * val;
      return NUMBER;
    }
    if (isalpha(ch)) {
      switch (ch) {
        case 'C':
		val = 0;
		break;
        case 'D':
		val = 2;
		break;
        case 'E':
		val = 4;
		break;
        case 'F':
		val = 5;
		break;
        case 'G':
		val = 7;
		break;
        case 'A':
		val = 9;
		break;
        case 'B':
		val = 11;
		break;
        case 'R':
		return REST;
        default:
		error("illegal name '%c' in line %d", ch, lineno);
		/* never reached */
		break;
      }
      ch = getc(scoreFile);
      if (ch == '#') {
        val++;
      } else
      if (ch == 'b') {
        val--;
      } else {
        ungetc(ch, scoreFile);
      }
      yylval = val;
      return NAME;
    }
    if (ch == ',') {
      return COMMA;
    }
    if (ch == ':') {
      return COLON;
    }
    if (ch == '.') {
      return DOT;
    }
    if (ch == '*') {
      return STAR;
    }
    if (ch == '+') {
      return PLUS;
    }
    if (ch == '@') {
      return AT;
    }
    if (ch == '$') {
      return DOLLAR;
    }
    if (ch == -1) {
      return -1;
    }
    error("illegal character 0x%02X in line %d", ch, lineno);
    /* never reached */
    return 0;
  }
}


/**************************************************************/


char *cmdName[] = {
  /* CMD_NONE     */    "      none",
  /* CMD_INSTR    */    "     instr",
  /* CMD_NOTE_ON  */    "   note on",
  /* CMD_NOTE_OFF */    "  note off",
  /* CMD_REST_ON  */    "   rest on",
  /* CMD_REST_OFF */    "  rest off",
  /* CMD_STOP     */    "      stop",
  /* CMD_TEMPO    */    "     tempo",
};


void showCommand(Command *cmdptr) {
  if (cmdptr->type < 0 ||
      cmdptr->type >= sizeof(cmdName) / sizeof(cmdName[0])) {
    error("unknown command type %d in showCommand()", cmdptr->type);
  }
  printf(" %9d %9d %s   %3d   %3d   %3d\n",
         cmdptr->sync, cmdptr->time, cmdName[cmdptr->type],
         cmdptr->note, cmdptr->octv, cmdptr->parm);
}


void showCommandList(Command *cmdptr) {
  while (cmdptr != NULL) {
    showCommand(cmdptr);
    cmdptr = cmdptr->next;
  }
}


void showCommandLists(void) {
  int i;

  for (i = 0; i < MAX_VOICES; i++) {
    printf("voice %2d:\n", i);
    printf("      sync      time       type  note  octv  parm  \n");
    printf(" --------------------------------------------------\n");
    showCommandList(cmdlists[i]);
    printf("\n");
  }
}


void prepCommandLists(void) {
  int i;
  Command *cmdptr;

  for (i = 0; i < MAX_VOICES; i++) {
    cmdptr = (Command *) allocate(sizeof(Command));
    cmdptr->sync = currSyncpoint;
    cmdptr->time = currSyncpoint;
    cmdptr->type = CMD_NONE;
    cmdptr->note = 0;
    cmdptr->octv = 0;
    cmdptr->parm = 0;
    cmdptr->next = NULL;
    cmdlists[i] = cmdptr;
    cmdlasts[i] = cmdptr;
  }
}


/**************************************************************/


char *eventName[] = {
  /* EVENT_NOTE_ON   */  "   note on",
  /* EVENT_NOTE_OFF  */  "  note off",
  /* EVENT_STOP      */  "      stop",
  /* EVENT_SET_INST  */  "  set inst",
  /* EVENT_SET_TEMPO */  " set tempo",
};


void showEvent(MusEvent *ptr) {
  if (ptr->event < 0 ||
      ptr->event >= sizeof(eventName) / sizeof(eventName[0])) {
    error("unknown event type %d in showEvent()", ptr->event);
  }
  printf(" %9d %s   %3d     %3d     %3d\n",
         ptr->time, eventName[ptr->event],
         ptr->voice, ptr->param1, ptr->param2);
}


int writeCmd(Command *ptr, int voice, FILE *musicFile, int format) {
  MusEvent event;

  event.time = ptr->time;
  switch (ptr->type) {
    case CMD_NONE:
      /* nothing generated */
      return 0;
    case CMD_INSTR:
      event.event = EVENT_SET_INST;
      event.voice = voice;
      event.param1 = ptr->parm;
      event.param2 = 0;
      break;
    case CMD_NOTE_ON:
      event.event = EVENT_NOTE_ON;
      event.voice = voice;
      event.param1 = ((ptr->octv & 0x07) << 4) | (ptr->note & 0x0F);
      event.param2 = 0;
      break;
    case CMD_NOTE_OFF:
      event.event = EVENT_NOTE_OFF;
      event.voice = voice;
      event.param1 = ((ptr->octv & 0x07) << 4) | (ptr->note & 0x0F);
      event.param2 = 0;
      break;
    case CMD_REST_ON:
      /* nothing generated */
      return 0;
    case CMD_REST_OFF:
      /* nothing generated */
      return 0;
    case CMD_STOP:
      event.event = EVENT_STOP;
      event.voice = voice;
      event.param1 = 0;
      event.param2 = 0;
      break;
    case CMD_TEMPO:
      event.event = EVENT_SET_TEMPO;
      event.voice = voice;
      event.param1 = ptr->parm;
      event.param2 = 0;
      break;
    default:
      error("unknown command type %d in writeCmd()", ptr->type);
      break;
  }
  if (showMusic) {
    showEvent(&event);
  }
  if (format == OFMT_BIN) {
    if (fwrite(&event, sizeof(MusEvent), 1, musicFile) != 1) {
      error("cannot write music file");
    }
  } else
  if (format == OFMT_ASM) {
    fprintf(musicFile, "\t.word\t0x%08X\n", event.time);
    fprintf(musicFile, "\t.byte\t0x%02X, 0x%02X\n",
            event.event, event.voice);
    fprintf(musicFile, "\t.byte\t0x%02X, 0x%02X\n",
            event.param1, event.param2);
  } else
  if (format == OFMT_C) {
    fprintf(musicFile, "  { ");
    fprintf(musicFile, "0x%08X, ", event.time);
    fprintf(musicFile, "0x%02X, 0x%02X, ", event.event, event.voice);
    fprintf(musicFile, "0x%02X, 0x%02X ", event.param1, event.param2);
    fprintf(musicFile, "},\n");
  }
  return 1;
}


void writeAsmHeader(FILE *musicFile, unsigned int numEvents) {
  fprintf(musicFile, "\t.code\n");
  fprintf(musicFile, "\t.align\t4\n");
  fprintf(musicFile, "\n");
  fprintf(musicFile, "\t.export\tmusicSize\n");
  fprintf(musicFile, "\t.export\tmusic\n");
  fprintf(musicFile, "\n");
  fprintf(musicFile, "musicSize:\n");
  fprintf(musicFile, "\t.word\t0x%08X\n", numEvents);
  fprintf(musicFile, "\n");
  fprintf(musicFile, "music:\n");
}


void generate(FILE *musicFile, int format) {
  int i;
  int voice;
  Command *ptr;
  MusHeader header;

  /* write preliminary header */
  header.musMagic = MUS_MAGIC;
  header.numEvents = 0;
  if (showMusic) {
    printf("music events:\n");
    printf("      time      event voice  param1  param2  \n");
    printf(" -------------------------------------------\n");
  }
  if (format == OFMT_BIN) {
    if (fwrite(&header, sizeof(MusHeader), 1, musicFile) != 1) {
      error("cannot write music file");
    }
  } else
  if (format == OFMT_ASM) {
    writeAsmHeader(musicFile, 0);
  } else
  if (format == OFMT_C) {
    /* nothing to do here */
  }
  while (1) {
    voice = -1;
    for (i = 0; i < MAX_VOICES; i++) {
      if (cmdlists[i] != NULL) {
        if (voice == -1 || cmdlists[i]->time < cmdlists[voice]->time) {
          voice = i;
        }
      }
    }
    if (voice == -1) {
      break;
    } else {
      ptr = cmdlists[voice];
      header.numEvents += writeCmd(ptr, voice, musicFile, format);
      cmdlists[voice] = ptr->next;
    }
  }
  /* write final header */
  fseek(musicFile, 0, SEEK_SET);
  if (format == OFMT_BIN) {
    if (fwrite(&header, sizeof(MusHeader), 1, musicFile) != 1) {
      error("cannot write music file");
    }
  } else
  if (format == OFMT_ASM) {
    writeAsmHeader(musicFile, header.numEvents);
  } else
  if (format == OFMT_C) {
    /* nothing to do here */
  }
}


int main(int argc, char *argv[]) {
  char *formatName;
  char *musicName;
  char *scoreName;
  int format;
  FILE *musicFile;

  if (argc != 4) {
    printf("Usage: %s -b|-s|-c <music file> <score file>\n", argv[0]);
    return 1;
  }
  formatName = argv[1];
  musicName = argv[2];
  scoreName = argv[3];
  if (strcmp(formatName, "-b") == 0) {
    format = OFMT_BIN;
  } else
  if (strcmp(formatName, "-s") == 0) {
    format = OFMT_ASM;
  } else
  if (strcmp(formatName, "-c") == 0) {
    format = OFMT_C;
  } else {
    error("output file format must be one of '-b', '-s', or '-c'");
  }
  scoreFile = fopen(scoreName, "r");
  if (scoreFile == NULL) {
    error("cannot open score file '%s'", scoreName);
  }
  prepCommandLists();
  yyparse();
  if (showLists) {
    showCommandLists();
  }
  fclose(scoreFile);
  musicFile = fopen(musicName, "w");
  if (musicFile == NULL) {
    error("cannot open music file '%s'", musicName);
  }
  generate(musicFile, format);
  fclose(musicFile);
  return 0;
}
