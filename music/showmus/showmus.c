/*
 * showmus.c -- show contents of music file
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>

#include "music.h"


unsigned int musicSize;
MusEvent *music;


void error(char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  printf("Error: ");
  vprintf(fmt, ap);
  printf("\n");
  va_end(ap);
  exit(1);
}


void loadMusic(char *fileName) {
  FILE *musicFile;
  MusHeader header;

  musicFile = fopen(fileName, "r");
  if (musicFile == NULL) {
    error("cannot open music file '%s'", fileName);
  }
  if (fread(&header, sizeof(MusHeader), 1, musicFile) != 1) {
    error("cannot read music file '%s'", fileName);
  }
  if (header.musMagic != MUS_MAGIC) {
    error("file '%s' is not a music file", fileName);
  }
  musicSize = header.numEvents;
  music = malloc(musicSize * sizeof(MusEvent));
  if (music == NULL) {
    error("not enough memory for music file '%s'", fileName);
  }
  if (fread(music, sizeof(MusEvent), musicSize, musicFile) != musicSize) {
    error("cannot read music file '%s'", fileName);
  }
  fclose(musicFile);
}


void showEvent(MusEvent *ptr) {
  printf("%10d: ", ptr->time);
  switch (ptr->event) {
    case EVENT_NOTE_ON:
      printf("note on,   voice = %2d, note = 0x%02X\n",
             ptr->voice, ptr->param1);
      break;
    case EVENT_NOTE_OFF:
      printf("note off,  voice = %2d\n",
             ptr->voice);
      break;
    case EVENT_STOP:
      printf("stop\n");
      break;
    case EVENT_SET_INST:
      printf("set inst,  voice = %2d, inst = %3d\n",
             ptr->voice, ptr->param1);
      break;
    case EVENT_SET_TEMPO:
      printf("set tempo, tempo = %3d\n",
             ptr->param1);
      break;
    default:
      error("unknown event %d in showEvent()", ptr->event);
      break;
  }
}


void showMusic(void) {
  int i;

  printf("%u events\n", musicSize);
  for (i = 0; i < musicSize; i++) {
    showEvent(music + i);
  }
}


int main(int argc, char *argv[]) {
  if (argc != 2 || *argv[1] == '-') {
    printf("Usage: %s <music file>\n", argv[0]);
    exit(1);
  }
  loadMusic(argv[1]);
  showMusic();
  return 0;
}
