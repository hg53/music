/*
 * music.h -- music file definition
 */


#ifndef _MUSIC_H_
#define _MUSIC_H_


#define MUS_MAGIC_V00		0x53554DA4
#define MUS_MAGIC_V01		0x53554DA5
#define MUS_MAGIC_V02		0x53554DA6
#define MUS_MAGIC_V03		0x53554DA7
#define MUS_MAGIC		MUS_MAGIC_V03


typedef struct {
  unsigned int musMagic;	/* must be MUS_MAGIC */
  unsigned int numEvents;	/* number of events */
} MusHeader;


#define EVENT_NOTE_ON		0
#define EVENT_NOTE_OFF		1
#define EVENT_STOP		2
#define EVENT_SET_INST		3
#define EVENT_SET_TEMPO		4


typedef struct {
  unsigned int time;
  unsigned char event;
  unsigned char voice;
  unsigned char param1;
  unsigned char param2;
} MusEvent;


#endif /* _MUSIC_H_ */
