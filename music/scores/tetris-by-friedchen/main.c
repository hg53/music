/*
 * main.c - main program
 */


#include "common.h"
#include "stdarg.h"
#include "iolib.h"
#include "start.h"
#include "../music/genmus/music.h"


#define MAJOR_VERSION	1
#define MINOR_VERSION	0

#define DEBUG_EVENTS	false
#define DEBUG_REGWRT	true

#define TMR_BASE	((Word *) 0xF0000000)
#define SYN_BASE	((Word *) 0xF0500000)

#define KON(v)		((v) * 16 + 0)
#define DELTA(v)	((v) * 16 + 1)
#define WAVE(v)		((v) * 16 + 2)
#define LEVEL(v)	((v) * 16 + 3)
#define AD(v)		((v) * 16 + 4)
#define SR(v)		((v) * 16 + 5)


/**************************************************************/


MusEvent music[] = {
  #include "piece.mus"
};
int musicSize = sizeof(music) / sizeof(music[0]);

Bool run;


/**************************************************************/


void error(char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  printf("Error: ");
  vprintf(fmt, ap);
  printf("\n");
  va_end(ap);
  while (1) ;
}


/**************************************************************/


void defaultISR(int irq, Word *registers) {
  printf("\n*** unexpected exception %d ***\n", irq);
}


void initInterrupts(void) {
  int i;

  for (i = 0; i < 32; i++) {
    setISR(i, defaultISR);
  }
  enable();
}


/**************************************************************/


#define CLKRT	50000000	/* clock rate: cycles per second */


static Word seconds;


void timerISR(int irq, Word *registers) {
  volatile Word *base;

  base = TMR_BASE;
  *(base + 0) = 0x02;
  seconds++;
}


void resetTimer(void) {
  volatile Word *base;

  base = TMR_BASE;
  disable();
  setISR(14, timerISR);
  orMask(1 << 14);
  seconds = 0;
  *(base + 1) = CLKRT;
  *(base + 0) = 0x02;
  enable();
}


void getTime(Word *secPtr, Word *cycPtr) {
  volatile Word *base;

  base = TMR_BASE;
  disable();
  *secPtr = seconds;
  *cycPtr = CLKRT - *(base + 2);
  enable();
}


/**************************************************************/


#define TPQ	480		/* resolution: ticks per quarter */


Word beatsPerMinute;
Word ticksPerSecond;
Word cyclesPerTick;


void setBeatsPerMinute(Word bpm) {
  beatsPerMinute = bpm;
  ticksPerSecond = (beatsPerMinute * TPQ) / 60;
  cyclesPerTick = CLKRT / ticksPerSecond;
}


Word getTicks(void) {
  Word seconds;
  Word cycles;

  getTime(&seconds, &cycles);
  return seconds * ticksPerSecond + cycles / cyclesPerTick;
}


/**************************************************************/


void regWrite(int reg, Word val) {
  volatile Word *base;

  if (DEBUG_REGWRT) {
    printf("%10d: reg %3d   <--   0x%08X\n",
           getTicks(), reg, val);
  }
  base = SYN_BASE;
  *(base + reg) = val;
}


Word regRead(int reg) {
  volatile Word *base;

  base = SYN_BASE;
  return *(base + reg);
}


/**************************************************************/


#define WF(duty,wave)	(((duty) << 8) | (wave))


typedef struct {
  unsigned int reg;
  unsigned int val;
} RegVal;

typedef struct {
  int n;
  RegVal regVal[16];
} Inst;

// LEAD1_1
Inst inst_000 = {
  4,
  {
    { WAVE(0),  WF(0xFF, 2) },
    { LEVEL(0), 0x0000 },
    { AD(0),    0xFF55 },
    { SR(0),    0xFFFF },
  }
};

// LEAD2_1
Inst inst_001 = {
  4,
  {
    { WAVE(0),  WF(0xFF, 2) },
    { LEVEL(0), 0x0000 },
    { AD(0),    0xFF55 },
    { SR(0),    0xFFFF },
  }
};

// BASS
Inst inst_002 = {
  4,
  {
    { WAVE(0),  WF(0x00, 1) },
    { LEVEL(0), 0x0000 },
    { AD(0),    0xFFFF },
    { SR(0),    0x00FF },
  }
};

// DRUM
Inst inst_003 = {
  4,
  {
    { WAVE(0),  WF(0x00, 3) },
    { LEVEL(0), 0x0000 },
    { AD(0),    0xFF70 },
    { SR(0),    0xFFFF },
  }
};


Inst inst_004 = {
  4,
  {
    { WAVE(0),  WF(0x00, 1) },
    { LEVEL(0), 0x0000 },
    { AD(0),    0xFFFF },
    { SR(0),    0x00FF },
  }
};


Inst inst_005 = {
  4,
  {
    { WAVE(0),  WF(0x00, 3) },
    { LEVEL(0), 0x0000 },
    { AD(0),    0xFF70 },
    { SR(0),    0xFFFF },
  }
};


Inst inst_006 = {
  4,
  {
    { WAVE(0),  WF(0x00, 0) },
    { LEVEL(0), 0x0000 },
    { AD(0),    0x0000 },
    { SR(0),    0x0000 },
  }
};


Inst inst_007 = {
  4,
  {
    { WAVE(0),  WF(0x00, 0) },
    { LEVEL(0), 0x0000 },
    { AD(0),    0x0000 },
    { SR(0),    0x0000 },
  }
};


Inst *instruments[] = {
  &inst_000,
  &inst_001,
  &inst_002,
  &inst_003,
  &inst_004,
  &inst_005,
  &inst_006,
  &inst_007,
};

int numInstruments = sizeof(instruments) / sizeof(instruments[0]);


/**************************************************************/


Word deltaTbl[12] = {
  2809, 2976, 3153, 3341, 3539, 3750,
  3973, 4209, 4459, 4724, 5005, 5303
};


void showEvent(MusEvent *ptr) {
  printf("%10d: ", ptr->time);
  switch (ptr->event) {
    case EVENT_NOTE_ON:
      printf("note on,  voice = %2d, note = 0x%02X\n",
             ptr->voice, ptr->param1);
      break;
    case EVENT_NOTE_OFF:
      printf("note off, voice = %2d\n",
             ptr->voice);
      break;
    case EVENT_STOP:
      printf("stop\n");
      break;
    case EVENT_SET_INST:
      printf("set inst, voice = %2d, inst = %3d\n",
             ptr->voice, ptr->param1);
      break;
    case EVENT_SET_TEMPO:
      printf("set tempo, tempo = %3d\n",
             ptr->param1);
      break;
    default:
      error("unknown event %d in showEvent()", ptr->event);
      break;
  }
}


void noteOn(MusEvent *ptr) {
  int voice;
  Word note;

  voice = ptr->voice;
  note = ptr->param1;
  regWrite(DELTA(voice), ((note & 0x70) << 9) | deltaTbl[note & 0x0F]);
  regWrite(KON(voice), 1);
}


void noteOff(MusEvent *ptr) {
  int voice;

  voice = ptr->voice;
  regWrite(KON(voice), 0);
}


void stop(MusEvent *ptr) {
  run = false;
}


void setInst(MusEvent *ptr) {
  int voice;
  int instr;
  Inst *p;
  int r;

  voice = ptr->voice;
  instr = ptr->param1;
  if (instr < 0 || instr >= numInstruments) {
    error("unknown instrument %d in setInst()", instr);
  }
  p = instruments[instr];
  for (r = 0; r < p->n; r++) {
    regWrite(voice * 16 + p->regVal[r].reg, p->regVal[r].val);
  }
}


void setTempo(MusEvent *ptr) {
  setBeatsPerMinute(ptr->param1);
}


typedef void (*Handler)(MusEvent *);


Handler handlers[] = {
  /* note on   */  noteOn,
  /* note off  */  noteOff,
  /* stop      */  stop,
  /* set inst  */  setInst,
  /* set tempo */  setTempo,
};


void handleEvent(MusEvent *ptr) {
  if (ptr->event >= sizeof(handlers) / sizeof(handlers[0])) {
    error("unknown event %d in handleEvent()", ptr->event);
  }
  if (handlers[ptr->event] == NULL) {
    printf("Warning: event %d not implemented in this player\n",
           ptr->event);
  } else {
    (*handlers[ptr->event])(ptr);
  }
}


/**************************************************************/


int main(void) {
  MusEvent *musicPtr;

  printf("Music Player Version %d.%d started\n",
         MAJOR_VERSION, MINOR_VERSION);
  initInterrupts();
  resetTimer();
  setBeatsPerMinute(120);
  musicPtr = music;
  run = true;
  while (run) {
    while (musicPtr->time <= getTicks()) {
      if (DEBUG_EVENTS) {
        showEvent(musicPtr);
      }
      handleEvent(musicPtr);
      if (!run) {
        break;
      }
      musicPtr++;
      if (musicPtr >= music + musicSize) {
        error("running out of music events");
      }
    }
  }
  printf("Music Player stopped\n");
  return 0;
}
