/*
noise_gen - A Verilog noise generator for a Spartan-3 FPGA
Copyright (C) 2017  Nils Friedchen

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
*/

`timescale 1ns / 1ps
module noise_gen(clk, rst, en, noise_sample);
	input clk, rst, en;
	output reg [15:0] noise_sample;
	
	parameter salt = 16'h1e92;
	
	reg [15:0] noise;
	
	initial begin
		noise_sample = salt;
	end
	
	always @(*) begin
		noise = noise_sample;
		
		noise = {noise[14:0], (noise[15]^noise[14]^noise[12]^noise[3])};
	end
	
	always @(posedge clk) begin
		if(rst) begin
			noise_sample <= salt;
		end else if(en) begin
			noise_sample <= noise;
		end
	end
endmodule
