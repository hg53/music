Score:
A "score" is the description of a piece of music. It consists
of a list of elements, separated by commas. An element is one
of "instrument", "measure", "tempo", "note", or "rest".

Instrument:
An instrument is specified for a voice by a dollar sign ($),
followed by the voice number, a colon, and the instrument number.
Voices are numbered starting at 1, as are instruments.

Measure:
A measure is specified by an asterisk (*), optionally followed
by a number, which is the number of the measure. Measures start
counting at 1.

Tempo:
The tempo is specified by an at-sign (@), followed by a number.
The number defines the duration of a quarter note (a crotchet)
in "beats per minute".

Note:
A note consists of three parts: voice, pitch, and duration. The
parts are separated by colons. The voices are numbered starting
at 1; the maximum voice number is 16. Each note belogs to a voice.
The pitch is specified as a note name (C, D, E, F, G, A, B),
optionally followed by a sharp (#) or a flat (b), followed by
an octave number, which may be positive, zero, or negative (so
that the concert pitch A440 will be written as A4). The duration
of a note is given by a number, optionally preceded by a plus (+),
and optionally followed by a dot (.). The number is, e.g., 4 for
a quarter note (a crotchet), 8 for an eighth note (a quaver), etc.
The plus denotes a slur from the previous note of this voice to
the present one. A dot multiplies the duration of a note by 1.5.

Rest:
The specification of a rest equals that of a note, where the pitch
is substituted by the single letter "R". Accidentals as well as the
octave number must not be present.
