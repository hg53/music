//
// dac_ctrl.v -- generate DAC control signals
//


`timescale 1ns/10ps
`default_nettype none


module dac_ctrl(clk, rst,
                next, sample_l, sample_r,
                mclk, bclk, daclrc, dacdat);
    input clk;
    input rst;
    output next;
    input [23:0] sample_l;
    input [23:0] sample_r;
    output mclk;
    output bclk;
    output daclrc;
    output dacdat;

  reg [9:0] timing;
  reg [63:0] sr;
  wire shift;

  always @(posedge clk) begin
    if (rst) begin
      timing[9:0] <= 10'd0;
    end else begin
      timing[9:0] <= timing[9:0] + 10'd1;
    end
  end

  assign mclk = timing[1];
  assign bclk = timing[3];
  assign daclrc = timing[9];

  assign next = (timing[9:0] == 10'h1FF) ? 1'b1 : 1'b0;
  assign shift = (timing[3:0] == 4'hF) ? 1'b1 : 1'b0;

  always @(posedge clk) begin
    if (rst) begin
      sr[63:0] <= 64'h0;
    end else begin
      if (next) begin
        sr[63:56] <= 8'h00;
        sr[55:32] <= sample_l[23:0];
        sr[31:24] <= 8'h00;
        sr[23: 0] <= sample_r[23:0];
      end else begin
        if (shift) begin
          sr[63:1] <= sr[62:0];
          sr[0] <= 1'b0;
        end
      end
    end
  end

  assign dacdat = sr[63];

endmodule
