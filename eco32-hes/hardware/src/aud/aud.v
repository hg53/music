//
// aud.v -- audio interface
//


`timescale 1ns/10ps
`default_nettype none


module aud(clk, rst,
           stb, we, addr,
           data_in, data_out, ack,
           dac_next, dac_sample_l, dac_sample_r,
           i2c_sclk, i2c_sdat,
           aud_mclk, aud_bclk,
           aud_daclrc, aud_dacdat);
    // internal bus interface
    input clk;
    input rst;
    input stb;
    input we;
    input addr;
    input [15:0] data_in;
    output [15:0] data_out;
    output ack;
    // internal DAC interface
    output dac_next;
    input [23:0] dac_sample_l;
    input [23:0] dac_sample_r;
    // external control interface
    output i2c_sclk;
    inout i2c_sdat;
    // external audio interface
    output aud_mclk;
    output aud_bclk;
    output aud_daclrc;
    output aud_dacdat;

  reg [15:0] buffer;
  reg start;
  reg rdy;
  reg err;
  wire done;
  wire error;

  always @(posedge clk) begin
    if (rst) begin
      buffer[15:0] <= 16'h0000;
      start <= 1'b0;
      rdy <= 1'b1;
      err <= 1'b0;
    end else begin
      if (rdy) begin
        // waiting for start
        if (stb & we & addr) begin
          buffer[15:0] <= data_in[15:0];
          start <= 1'b1;
          rdy <= 1'b0;
          err <= 1'b0;
        end
      end else begin
        // waiting for end
        start <= 1'b0;
        if (done) begin
          rdy <= 1'b1;
          err <= error;
        end
      end
    end
  end

  assign data_out[15:0] = ~addr ? { 13'h0, err, 1'b0, rdy } :
                                  buffer[15:0];
  assign ack = stb;

  i2c_mc i2c_mc_0(
    .clk(clk),
    .rst(rst),
    .data(buffer[15:0]),
    .start(start),
    .done(done),
    .error(error),
    .i2c_scl(i2c_sclk),
    .i2c_sda(i2c_sdat)
  );

  dac_ctrl dac_ctrl_0(
    .clk(clk),
    .rst(rst),
    .next(dac_next),
    .sample_l(dac_sample_l[23:0]),
    .sample_r(dac_sample_r[23:0]),
    .mclk(aud_mclk),
    .bclk(aud_bclk),
    .daclrc(aud_daclrc),
    .dacdat(aud_dacdat)
  );

endmodule
