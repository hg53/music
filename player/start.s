;
; start.s -- startup, basic I/O, interrupt control
;

	.set	PSW,0
	.set	IEN,(1 << 23)

	.import	_bcode
	.import	_ecode
	.import	_bdata
	.import	_edata
	.import	_bbss
	.import	_ebss

	.import	main

	.export	getc
	.export	putc
	.export	enable
	.export	disable
	.export	orMask
	.export	andMask
	.export	getISR
	.export	setISR

	.code
	.align	4

	;
	; program startup
	;
start:
	; copy data segment
	add	$10,$0,_bdata		; lowest dst addr to be written to
	add	$8,$0,_edata		; one above the top dst addr
	sub	$9,$8,$10		; $9 = size of data segment
	add	$9,$9,_ecode		; data is waiting right after code
	j	cpytest
cpyloop:
	ldw	$11,$9,0		; src addr in $9
	stw	$11,$8,0		; dst addr in $8
cpytest:
	sub	$8,$8,4			; downward
	sub	$9,$9,4
	bgeu	$8,$10,cpyloop

	; clear bss segment
	add	$8,$0,_bbss		; start with first word of bss
	add	$9,$0,_ebss		; this is one above the top
	j	clrtest
clrloop:
	stw	$0,$8,0			; dst addr in $8
	add	$8,$8,4			; upward
clrtest:
	bltu	$8,$9,clrloop

	; set stack
	add	$29,$0,stack

	; capture interrupts
	add	$8,$0,isr
	sub	$8,$8,0xC0000004+4
	slr	$8,$8,2
	or	$8,$8,0xA8000000
	stw	$8,$0,0xC0000004

	; call main
	jal	main

	; halt execution by looping
stop:
	j	stop

	;
	; console input
	;
	; char getc(void);
	;
getc:
	add	$8,$0,0xC0000018
	jr	$8

	;
	; console output
	;
	; void putc(char c);
	;
putc:
	add	$8,$0,0xC0000020
	jr	$8

	;
	; enable interrupts
	;
	; void enable(void);
	;
enable:
	mvfs	$8,PSW
	or	$8,$8,IEN
	mvts	$8,PSW
	jr	$31

	;
	; disable interrupts
	;
	; void disable(void);
	;
disable:
	mvfs	$8,PSW
	and	$8,$8,~IEN
	mvts	$8,PSW
	jr	$31

	;
	; or into interrupt mask
	;
	; void orMask(Word mask);
	;
orMask:
	mvfs	$8,PSW
	and	$4,$4,0x0000FFFF	; use lower 16 bits only
	or	$8,$8,$4
	mvts	$8,PSW
	jr	$31

	;
	; and into interrupt mask
	;
	; void andMask(Word mask);
	;
andMask:
	mvfs	$8,PSW
	or	$4,$4,0xFFFF0000	; use lower 16 bits only
	and	$8,$8,$4
	mvts	$8,PSW
	jr	$31

	;
	; get ISR
	;
	; ISR getISR(int irq);
	;
getISR:
	sll	$4,$4,2
	ldw	$2,$4,irqsrv
	jr	$31

	;
	; set ISR
	;
	; void setISR(int irq, ISR isr);
	;
setISR:
	sll	$4,$4,2
	stw	$5,$4,irqsrv
	jr	$31

	;
	; main ISR entry
	;
isr:
	sub	$29,$29,128		; save registers
	stw	$0,$29,0
	stw	$1,$29,4
	stw	$2,$29,8
	stw	$3,$29,12
	stw	$4,$29,16
	stw	$5,$29,20
	stw	$6,$29,24
	stw	$7,$29,28
	stw	$8,$29,32
	stw	$9,$29,36
	stw	$10,$29,40
	stw	$11,$29,44
	stw	$12,$29,48
	stw	$13,$29,52
	stw	$14,$29,56
	stw	$15,$29,60
	stw	$16,$29,64
	stw	$17,$29,68
	stw	$18,$29,72
	stw	$19,$29,76
	stw	$20,$29,80
	stw	$21,$29,84
	stw	$22,$29,88
	stw	$23,$29,92
	stw	$24,$29,96
	stw	$25,$29,100
	stw	$26,$29,104
	stw	$27,$29,108
	stw	$28,$29,112
	stw	$29,$29,116
	stw	$30,$29,120
	stw	$31,$29,124
	add	$5,$29,$0		; $5 = pointer to register array
	mvfs	$4,0			; $4 = IRQ number
	slr	$4,$4,16
	and	$4,$4,0x1F
	sll	$8,$4,2			; $8 = 4 * IRQ number
	ldw	$8,$8,irqsrv		; get addr of service routine
	jalr	$8			; call service routine
	ldw	$1,$29,4		; restore registers
	ldw	$2,$29,8
	ldw	$3,$29,12
	ldw	$4,$29,16
	ldw	$5,$29,20
	ldw	$6,$29,24
	ldw	$7,$29,28
	ldw	$8,$29,32
	ldw	$9,$29,36
	ldw	$10,$29,40
	ldw	$11,$29,44
	ldw	$12,$29,48
	ldw	$13,$29,52
	ldw	$14,$29,56
	ldw	$15,$29,60
	ldw	$16,$29,64
	ldw	$17,$29,68
	ldw	$18,$29,72
	ldw	$19,$29,76
	ldw	$20,$29,80
	ldw	$21,$29,84
	ldw	$22,$29,88
	ldw	$23,$29,92
	ldw	$24,$29,96
	ldw	$25,$29,100
	ldw	$26,$29,104
	ldw	$27,$29,108
	ldw	$28,$29,112
	ldw	$30,$29,120
	ldw	$31,$29,124
	add	$29,$29,128
	rfx				; return from exception

	.data
	.align	4

irqsrv:
	.word	0		; 00: terminal 0 transmitter interrupt
	.word	0		; 01: terminal 0 receiver interrupt
	.word	0		; 02: terminal 1 transmitter interrupt
	.word	0		; 03: terminal 1 receiver interrupt
	.word	0		; 04: keyboard interrupt
	.word	0		; 05: unused
	.word	0		; 06: unused
	.word	0		; 07: unused
	.word	0		; 08: unused
	.word	0		; 09: unused
	.word	0		; 10: unused
	.word	0		; 11: unused
	.word	0		; 12: unused
	.word	0		; 13: unused
	.word	0		; 14: timer 0 interrupt
	.word	0		; 15: timer 1 interrupt
	.word	0		; 16: bus timeout exception
	.word	0		; 17: illegal instruction exception
	.word	0		; 18: privileged instruction exception
	.word	0		; 19: divide instruction exception
	.word	0		; 20: trap instruction exception
	.word	0		; 21: TLB miss exception
	.word	0		; 22: TLB write exception
	.word	0		; 23: TLB invalid exception
	.word	0		; 24: illegal address exception
	.word	0		; 25: privileged address exception
	.word	0		; 26: unused
	.word	0		; 27: unused
	.word	0		; 28: unused
	.word	0		; 29: unused
	.word	0		; 30: unused
	.word	0		; 31: unused

	.bss
	.align	4

	;
	; stack
	;
	.space	0x10000
stack:
