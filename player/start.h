/*
 * start.h -- startup, basic I/O, interrupt control
 */


#ifndef _start_h_
#define _start_h_


typedef void (*ISR)(int irq, Word *registers);


char getc(void);
void putc(char c);

void enable(void);
void disable(void);
void orMask(Word mask);
void andMask(Word mask);
ISR getISR(int irq);
void setISR(int irq, ISR isr);


#endif /* _start_h_ */
